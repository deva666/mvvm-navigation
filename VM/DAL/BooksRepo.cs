﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using VM.Models;
using VM.DAL;
using VM.Utils;

namespace VM.DAL
{
    class BooksRepo : IRepo<Book>
    {
        private List<Book> _books;
        private static Lazy<BooksRepo> _instance = new Lazy<BooksRepo>(()=> new BooksRepo());

        public static BooksRepo Instance
        {
            get
            {
                return _instance.Value;
            }
        }

        private BooksRepo()
        {
            _books = GetBooks(Constants.BOOKS_XML_FILE);
        }

        private List<Book> GetBooks(string xmlFile)
        {
            using (XmlReader reader = new XmlTextReader(xmlFile))
            {
                return (from book in XDocument.Load(reader).Element("books").Elements("book")
                        select new Book()
                        {
                            Title = (string)book.Attribute("title"),
                            ISBN = (string)book.Attribute("isbn"),
                            YearPublished = (string)book.Attribute("yearPublished"),
                            Author = (string)book.Attribute("author")
                        }).ToList();
            }
        }

        public List<Book> GetContent()
        {
            return _books;
        }
    }
}
