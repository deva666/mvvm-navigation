﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using VM.Models;
using VM.MVVM;
using VM.ViewModels;

namespace VM.VMWeakRef
{
    abstract class HostViewModel : ViewModelBase
    {
        protected IChildVM _selectedChild;

        protected ICommand _changePageCommand;

        public IChildVM SelectedChild
        {
            get
            {
                return _selectedChild;
            }
            set
            {
                _selectedChild = value;
                OnPropertyChanged("SelectedChild");
            }
        }

        public ICommand ChangePageCommand
        {
            get
            {
                return _changePageCommand == null ? _changePageCommand = new DelegateCommand(ChangePage) : _changePageCommand;
            }
        }

        public List<WeakReference<IChildVM>> Children { get; protected set; }

        protected virtual void ChangePage(object param)
        {
            Contract.Requires(param != null, "Parameter is null");
            Contract.Requires(param.GetType() == typeof(string), "Parameter must be string");
            Contract.Ensures(this.SelectedChild != null, "Selected child is null");

            string title = (string)param;
            IChildVM childVM = null;
            //remove all VM that got GC'd
            Children.RemoveAll(w => !w.TryGetTarget(out childVM));
            foreach (var c in Children)
            {
                if (c.TryGetTarget(out childVM))
                    if (childVM.Title == title)
                    {
                        SelectedChild = childVM;
                        return;
                    }
            }

            switch (title.ToUpper())
            {
                case "AUTHORS":
                    childVM = new AuthorsViewModel();
                    break;
                case "BOOKS":
                    childVM = new BooksViewModel();
                    break;
                default:
                    break;
            }
            SelectedChild = childVM;
            Children.Add(new WeakReference<IChildVM>(childVM));

        }
    }
}
