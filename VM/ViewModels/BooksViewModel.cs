﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VM.DAL;
using VM.Models;
using VM.MVVM;

namespace VM.ViewModels
{
    class BooksViewModel : ViewModelBase, IChildVM
    {
        IRepo<Book> _repo;

        public string Title
        {
            get
            {
                return "Books";
            }                        
        }

        public BooksViewModel()
        {
            _repo = BooksRepo.Instance;            
        }

        public List<Book> ContentSource
        {
            get
            {
               return _repo == null ? null : _repo.GetContent();
            }
        }
    }
}
