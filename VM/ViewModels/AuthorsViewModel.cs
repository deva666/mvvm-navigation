﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VM.DAL;
using VM.Models;
using VM.MVVM;


namespace VM.ViewModels
{
    class AuthorsViewModel : ViewModelBase , IChildVM
    {
        IRepo<Author> _repo;

        public string Title
        {
            get { return "Authors"; }
        }

        public AuthorsViewModel()
        {
            _repo = AuthorsRepo.Instance;
        }

        public List<Author> ContentSource
        {
            get
            {
                return _repo == null ? null : _repo.GetContent();
            }
        }
    }
}
