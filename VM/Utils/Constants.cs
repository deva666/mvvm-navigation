﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VM.Utils
{
    class Constants
    {
        public const string BOOKS_XML_FILE = @"Data\books.xml";
        public const string AUTHORS_XML_FILE = @"Data\authors.xml";
    }
}
