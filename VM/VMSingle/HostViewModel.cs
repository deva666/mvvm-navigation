﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using VM.Models;
using VM.MVVM;
using VM.ViewModels;

namespace VM.VMSingle
{
    abstract class HostViewModel : ViewModelBase
    {
        protected IChildVM _selectedChild;

        protected ICommand _changePageCommand;

        public IChildVM SelectedChild
        {
            get
            {
                return _selectedChild;
            }
            set
            {
                if (value == null)
                    return;

                if (_selectedChild != null)
                    ((ViewModelBase)_selectedChild).Dispose();
                _selectedChild = value;
                OnPropertyChanged("SelectedChild");

            }
        }

        public ICommand ChangePageCommand
        {
            get
            {
                return _changePageCommand == null ? _changePageCommand = new DelegateCommand(ChangePage) : _changePageCommand;
            }
        }


        protected virtual void ChangePage(object param)
        {
            Contract.Requires(param != null, "Parameter is null");
            Contract.Requires(param.GetType() == typeof(string), "Parameter must be string");
            Contract.Ensures(this.SelectedChild != null, "Selected child is null");

            string title = ((string)param).ToUpper();

            switch (title)
            {
                case "AUTHORS":
                    if (this.SelectedChild == null || this.SelectedChild.Title.ToUpper() != title)
                        this.SelectedChild = new AuthorsViewModel();
                    break;
                case "BOOKS":
                    if (this.SelectedChild == null || this.SelectedChild.Title.ToUpper() != title)
                        this.SelectedChild = new BooksViewModel();
                    break;
                default:
                    break;
            }

            
        }
    }
}
