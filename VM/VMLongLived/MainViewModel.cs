﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VM.Models;
using VM.MVVM;
using VM.ViewModels;

namespace VM.VMLongLived
{
    class MainViewModel : HostViewModel
    {
        Timer _timer;

        public string Version
        {
            get
            {
                return this.GetType().Assembly.GetName().Version.ToString();
            }
        }

        private string _time;
        public string Time
        {
            get
            {
                return _time;
            }
            set
            {
                _time = value;
                OnPropertyChanged("Time");
            }
        }

        public MainViewModel()
        {
            IChildVM books = new BooksViewModel();
            List<IChildVM> children = new List<IChildVM>();
            //Lazy load, i.e. construct only the first VM add the other if user ever navigates to them
            children.Add(books);
            this.Children = children;
            SelectedChild = books;

            _timer = new Timer((s) => Time = DateTime.Now.ToLongTimeString(), this, 500, 500);
        }

        protected override void OnDispose()
        {
            foreach (var vm in this.Children)
            {
                ((ViewModelBase)vm).Dispose();
            }
            _timer.Dispose();
            base.OnDispose();
        }
    }
}
