﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using VM.Models;
using VM.MVVM;
using VM.ViewModels;

namespace VM.VMLongLived
{
    abstract class HostViewModel : ViewModelBase
    {
        protected IChildVM _selectedChild;

        protected ICommand _changePageCommand;

        public IChildVM SelectedChild
        {
            get
            {
                return _selectedChild;
            }
            set
            {
                _selectedChild = value;
                OnPropertyChanged("SelectedChild");
            }
        }

        public ICommand ChangePageCommand
        {
            get
            {
                return _changePageCommand == null ? _changePageCommand = new DelegateCommand(ChangePage) : _changePageCommand;
            }
        }

        public List<IChildVM> Children { get; protected set; }

        protected virtual void ChangePage(object param)
        {
            Contract.Requires(param != null, "Parameter is null");
            Contract.Requires(param.GetType() == typeof(string), "Parameter must be string");
            Contract.Ensures(this.SelectedChild != null, "Selected child is null");

            string title = (string)param;
            IChildVM childVM = Children.Where(c => c.Title == title).SingleOrDefault();
            if (childVM == null)
            {
                switch (title.ToUpper())
                {
                    case "AUTHORS":
                        childVM = new AuthorsViewModel();
                        break;
                    case "BOOKS":
                        childVM = new BooksViewModel();
                        break;
                    default:
                        break;
                }
                Children.Add(childVM);
            }
            SelectedChild = childVM;
        }
    }
}
