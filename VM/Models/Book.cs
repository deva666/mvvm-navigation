﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VM.Models
{
    class Book : Model
    {
        public string Title { get; set; }
        public string ISBN { get; set; }
        public string YearPublished { get; set; }
        public string Author { get; set; }

    }
}
