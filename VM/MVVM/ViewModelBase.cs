﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VM.MVVM
{
    abstract class ViewModelBase : ObservableObject, IDisposable
    {
        protected virtual void OnDispose()
        {
            Console.WriteLine(string.Format("Disposing {0} , {1}", this.GetType().Name, this.GetType().FullName));
        }

        public void Dispose()
        {
            OnDispose();
        }

    }
}
